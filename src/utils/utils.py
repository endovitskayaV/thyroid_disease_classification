from typing import Any

import pandas
from pandas import DataFrame


def read_data(file_name: str) -> DataFrame:
    return pandas.read_csv(file_name, index_col=False)


def write_to_file(filename: str, content: Any) -> None:
    with open(filename, 'w') as file:
        file.write(str(content))
