import click
import pandas
import statsmodels.api as sm
from pandas import DataFrame
from statsmodels.stats.proportion import proportions_ztest, proportion_confint, confint_proportions_2indep

from src.data.make_dataset import MAPPED_CSV, IMPUTED_CSV


def read_data(file_name: str) -> DataFrame:
    return pandas.read_csv(file_name, index_col=False)


@click.command()
@click.argument('preprocessed_filepath', type=click.Path())
@click.argument('compare_imputed', type=click.Path())
def compare_imputed(preprocessed_filepath: str) -> None:
    initial_df = read_data(preprocessed_filepath + MAPPED_CSV)
    imputed_df = read_data(preprocessed_filepath + IMPUTED_CSV)
    columns_to_plot = ['FTI', 'T3', 'TSH', 'TT4', 'sex']
    for col in columns_to_plot:
        df = pandas.DataFrame({
            'initial': initial_df[col],
            'imputed': imputed_df[col]
        })
        ax = df.plot.kde()
        ax.figure.savefig(col + '_kde.png')


@click.command()
@click.argument('preprocessed_filepath', type=click.Path())
def test_sex_hypethesis(preprocessed_filepath: str) -> None:
    df = read_data(preprocessed_filepath + IMPUTED_CSV)[['sex', 'label']]
    df['label'] = df['label'].map(lambda label: 1 if label == 'hyper' else 0)
    females = df[df['sex'] == 0]['label']
    males = df[df['sex'] == 1]['label']
    females_amount = females.count()
    males_amount = males.count()
    females_successes = females.sum()
    males_successes = males.sum()
    successes = [females_successes, males_successes]
    nobs = [females_amount, males_amount]

    z_stat, pval = proportions_ztest(successes, nobs=nobs, alternative='larger')

    print('z statistic', z_stat)
    print('p-value', pval)


if __name__ == '__main__':
    compare_imputed()
    test_sex_hypethesis()
