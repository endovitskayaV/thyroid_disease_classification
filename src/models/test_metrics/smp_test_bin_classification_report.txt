              precision    recall  f1-score   support

    negative       0.99      0.86      0.92       876
    positive       0.42      0.96      0.58        95

    accuracy                           0.87       971
   macro avg       0.71      0.91      0.75       971
weighted avg       0.94      0.87      0.89       971
