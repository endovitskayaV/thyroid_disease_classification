from collections import Counter
from typing import List

import click
import joblib
import pandas
from imblearn.ensemble import BalancedRandomForestClassifier
from imblearn.over_sampling import RandomOverSampler
from pandas import DataFrame, Series
from sklearn.model_selection import GridSearchCV

from src.data.make_dataset import LABEL, DATA_PREFIX, NEGATIVE, HYPO, GOITRE, HYPERTHYROID, T_TOXIC, FINAL_CSV
from src.utils.utils import read_data, write_to_file

ALL_X_COLUMNS = ['age', 'sex', 'on thyroxine', 'query on thyroxine', 'on antithyroid medication', 'sick',
                        'pregnant',
                        'thyroid surgery', 'I131 treatment', 'query hypothyroid', 'query hyperthyroid', 'lithium',
                        'goitre',
                        'tumor', 'hypopituitary', 'psych', 'TSH measured', 'TSH', 'T3 measured', 'T3', 'TT4 measured',
                        'TT4', 'T4U measured', 'T4U', 'FTI measured', 'FTI', 'referral source']

ALL_PREFIX = 'all-'

SAMPLING_PREFIX = 'smp_'
MULTI_CLF_DUMP_FILE = "multi_clf_dump_file.pkl"
BINARY_CLF_DUMP_FILE = "binary_clf_dump_file.pkl"
POSITIVE = 'positive'
MAIN_FEATURES = ['TSH', 'FTI', 'TT4', 'T3', 'T4U']
prefixes = ['', SAMPLING_PREFIX]


@click.command()
@click.argument('preprocessed_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
@click.argument('dump_filepath', type=click.Path())
def do_train_model(preprocessed_filepath: str, output_filepath: str, dump_filepath: str) -> None:
    df = read_data(preprocessed_filepath + DATA_PREFIX + FINAL_CSV)
    write_to_file(output_filepath + 'counter.txt', Counter(df[LABEL]))

    for prefix in prefixes:
        # do_fit(df, ALL_X_COLUMNS, ALL_PREFIX + prefix, output_filepath, dump_filepath, False)

        do_fit(df, MAIN_FEATURES, prefix, output_filepath, dump_filepath, prefix == SAMPLING_PREFIX)


def do_fit(df: DataFrame, x_columns: List[str], prefix: str, output_filepath: str, dump_filepath: str,
           do_sampling: bool) -> None:
    binary_clf = train_binary_classifier(df, prefix, x_columns, output_filepath)
    multi_clf = train_multi_classifier(df, do_sampling, prefix, x_columns,
                                       output_filepath)
    save_classifiers(binary_clf, multi_clf, dump_filepath, prefix)


def save_classifiers(binary_clf, multi_clf,
                     dump_filepath: str, prefix: str) -> None:
    joblib.dump(binary_clf, dump_filepath + prefix + BINARY_CLF_DUMP_FILE)
    joblib.dump(multi_clf, dump_filepath + prefix + MULTI_CLF_DUMP_FILE)


def train_multi_classifier(df: DataFrame, do_sampling: bool, prefix: str, x_columns: List[str],
                           output_filepath: str):
    df_positive: DataFrame = df[df[LABEL] != NEGATIVE]
    X: DataFrame = df_positive[x_columns]
    y: Series = df_positive[LABEL]
    if do_sampling:
        max_amount: int = Counter(y).most_common()[0][1]
        over_sampler: RandomOverSampler = RandomOverSampler(
            {HYPERTHYROID: max_amount // 2, HYPO: max_amount, GOITRE: max_amount // 2,
             T_TOXIC: max_amount // 2})
        X, y = over_sampler.fit_resample(X, y)
        write_to_file(output_filepath + prefix + 'over_sampling_counter.txt', Counter(y))
    multi_clf = train_clf(x_columns, X, y, prefix + 'multi_', output_filepath)
    return multi_clf


def train_binary_classifier(df: DataFrame, prefix: str, x_columns: List[str],
                            output_filepath: str):
    X: DataFrame = df[x_columns]
    y: Series = df[LABEL]
    y = y.map(lambda label: NEGATIVE if label == NEGATIVE else POSITIVE)
    binary_clf = train_clf(x_columns, X, y, prefix, output_filepath)
    return binary_clf


def train_clf(columns: List[str], X_train: DataFrame, y_train: Series, prefix: str,
              output_filepath: str):
    param_grid = {
        'n_estimators': [50, 100, 200, 500],
        'max_depth': [5, 10, 20, 50],
        'max_features': [1, 3, 5]
    }
    grid_cv = GridSearchCV(estimator=BalancedRandomForestClassifier(), param_grid=param_grid, cv=5)
    grid_cv.fit(X_train, y_train)
    write_to_file(output_filepath + prefix + 'best_params.txt', grid_cv.best_params_)
    clf = BalancedRandomForestClassifier(n_estimators=grid_cv.best_params_['n_estimators'],
                                         max_depth=grid_cv.best_params_['max_depth'], max_features=grid_cv.best_params_['max_features'])
    clf.fit(X_train, y_train)
    get_important_features(clf, columns, prefix, output_filepath)
    return clf


def get_important_features(clf, columns: List[str], prefix: str, output_filepath: str) -> None:
    important_features: Series = pandas.Series(data=clf.feature_importances_, index=columns)
    important_features.sort_values(ascending=False, inplace=True)
    write_to_file(output_filepath + prefix + 'important_features.txt', important_features)


if __name__ == '__main__':
    do_train_model()
