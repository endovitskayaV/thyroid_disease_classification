FTI                          0.192310
TSH                          0.184827
TT4                          0.175207
tumor                        0.173877
T3                           0.126052
age                          0.046340
T4U                          0.040297
referral source              0.033396
on thyroxine                 0.005095
T3 measured                  0.004044
query hyperthyroid           0.003276
pregnant                     0.003167
query hypothyroid            0.003025
on antithyroid medication    0.002985
sex                          0.002529
T4U measured                 0.001289
I131 treatment               0.000749
TSH measured                 0.000694
FTI measured                 0.000342
sick                         0.000257
query on thyroxine           0.000242
psych                        0.000000
hypopituitary                0.000000
goitre                       0.000000
lithium                      0.000000
thyroid surgery              0.000000
TT4 measured                 0.000000
dtype: float64