from collections import Counter

import click
import joblib
import numpy
import pandas
from matplotlib import pyplot as plt
from pandas import DataFrame, Series
from sklearn.metrics import f1_score, classification_report, roc_curve, auc, ConfusionMatrixDisplay, \
    confusion_matrix

from src.data.make_dataset import LABEL, TEST_PREFIX, NEGATIVE, FINAL_CSV
from src.models.train_model import BINARY_CLF_DUMP_FILE, MULTI_CLF_DUMP_FILE, prefixes, ALL_PREFIX, \
    ALL_X_COLUMNS, POSITIVE, MAIN_FEATURES
from src.utils.utils import write_to_file, read_data


@click.command()
@click.argument('preprocessed_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
@click.argument('dump_filepath', type=click.Path(exists=True))
def test(preprocessed_filepath: str, output_filepath: str, dump_filepath: str) -> None:
    df_test: DataFrame = read_data(preprocessed_filepath + TEST_PREFIX + FINAL_CSV)
    write_to_file(output_filepath + 'counter.txt', Counter(df_test[LABEL]))

    for prefix in prefixes:
        binary_clf = joblib.load(dump_filepath +  prefix + BINARY_CLF_DUMP_FILE)
        multi_clf = joblib.load(dump_filepath +  prefix + MULTI_CLF_DUMP_FILE)
        do_test(df_test, binary_clf, multi_clf, prefix + TEST_PREFIX + '_', output_filepath)


def do_test(df_test: DataFrame, binary_clf, multi_clf, prefix: str, output_filepath: str) -> None:
    X_test: DataFrame = df_test[MAIN_FEATURES]
    y_test: Series = df_test[LABEL]

    y_test_binary = y_test.map(lambda label: NEGATIVE if label == NEGATIVE else POSITIVE)
    calculate_metrics(y_test_binary, binary_clf.predict(X_test), binary_clf.predict_proba(X_test), binary_clf.classes_, prefix+'bin_', output_filepath)

    df_positive: DataFrame = df_test[df_test[LABEL] != NEGATIVE]
    X_test_multi_only: DataFrame = df_positive[MAIN_FEATURES]
    y_test_multi_only: Series = df_positive[LABEL]
    calculate_metrics(y_test_multi_only, multi_clf.predict(X_test_multi_only), multi_clf.predict_proba(X_test_multi_only), multi_clf.classes_, prefix+'pos_', output_filepath)

    classes = get_classes(binary_clf, multi_clf)
    classes_amount: int = classes.size
    calculate_metrics(y_test, pred(X_test, binary_clf, multi_clf), predict_proba(X_test, binary_clf, multi_clf, classes_amount), classes, prefix+'multi_', output_filepath)

def calculate_metrics(y_test: Series, y_pred:Series, probs, classes, prefix: str, output_filepath: str) -> None:
    write_to_file(output_filepath + prefix + 'f1.txt', f1_score(y_test, y_pred, average='weighted'))
    write_to_file(output_filepath + prefix + 'classification_report.txt', classification_report(y_test, y_pred))
    multiclass_roc_auc(classes, probs, y_test, prefix, output_filepath)
    confusion_matrix_(y_pred, y_test, classes, prefix, output_filepath)


def get_classes(binary_clf, multi_clf):
    classes = numpy.array([])
    classes = numpy.append(classes, binary_clf.classes_[0])
    classes = numpy.append(classes, multi_clf.classes_)
    return classes


def pred(X_test: DataFrame, binary_clf, multi_clf):
    y_pred = binary_clf.predict(X_test)
    for idx, pred in enumerate(y_pred):
        if pred != NEGATIVE:
            clf_pred = multi_clf.predict(X_test.iloc[[idx]])[0]
            y_pred[idx] = clf_pred
    return y_pred


def predict_proba(X_test: DataFrame, binary_clf, multi_clf, classes_amount: int):
    y_pred = []
    predictions_1 = binary_clf.predict_proba(X_test)

    for idx, pred in enumerate(predictions_1):
        pred_prob = numpy.zeros(classes_amount)
        if numpy.argmax(pred) == 0:
            pred_prob[0] = pred[0]
            pred_prob[1:] = pred[1]
        else:
            clf_pred = multi_clf.predict_proba(X_test.iloc[[idx]])[0]
            pred_prob[1:] = clf_pred
            pred_prob[0] = pred[0]
        y_pred.append(pred_prob)
    return numpy.array(y_pred)


def confusion_matrix_(y_pred, y_test, classes, prefix: str, output_filepath: str) -> None:
    plt.clf()
    fig, ax = plt.subplots(figsize=(12, 8))
    cm = confusion_matrix(y_test, y_pred)
    cmp = ConfusionMatrixDisplay(cm, display_labels=classes)
    cmp.plot(ax=ax)
    ax.figure.savefig(output_filepath + prefix + 'confusion_matrix.png')


def multiclass_roc_auc(classes, probs, y_test, prefix: str, output_filepath: str) -> None:
    plt.clf()
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    y_true: DataFrame = pandas.get_dummies(y_test, columns=classes)
    for i in range(classes.size):
        fpr[i], tpr[i], thresholds = roc_curve(y_true.iloc[:, i], probs[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
        plt.plot(fpr[i], tpr[i], label=classes[i] + ' (area = %s)' % roc_auc[i])
        plt.plot([0, 1], [0, 1], 'k--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC curve')
        plt.legend(loc="lower right")
        plt.savefig(output_filepath + prefix + 'roc_auc.png')


if __name__ == '__main__':
    test()
