# -*- coding: utf-8 -*-
import logging
from pathlib import Path

import click
import numpy
import pandas
from dotenv import find_dotenv, load_dotenv
from pandas import DataFrame, Series, Index
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from typing import List

REFERRAL_SOURCE_COLUMN = 'referral source'
SEX_COLUMN = 'sex'
CATEGORY_COLUMNS = [SEX_COLUMN, REFERRAL_SOURCE_COLUMN]

GOITRE = 'goitre'
T_TOXIC = 'T3 toxic'
HYPERTHYROID = 'hyperthyroid'
HYPER_LABELS = [HYPERTHYROID, T_TOXIC, GOITRE]
NEGATIVE = 'negative'
HYPER = 'hyper'
HYPO = 'hypo'

AGE_COLUMN = 'age'
ID_COLUMN = 'id'
LABEL = 'label'

DATA_PREFIX = 'data'
TEST_PREFIX = 'test'

MERGED_CSV = '_merged.csv'
MAPPED_CSV = '_mapped.csv'
FINAL_CSV = '.csv'

TBG_COLUMNS = ['TBG measured', 'TBG']
BOOLEAN_COLUMNS = ['on thyroxine', 'query on thyroxine', 'on antithyroid medication', 'sick', 'pregnant',
                   'thyroid surgery', 'I131 treatment', 'query hypothyroid', 'query hyperthyroid',
                   'lithium', 'goitre', 'tumor', 'hypopituitary', 'psych', 'TSH measured', 'T3 measured',
                   'TT4 measured', 'T4U measured', 'FTI measured']
X_COLUMN_NAMES = [AGE_COLUMN, SEX_COLUMN, 'on thyroxine', 'query on thyroxine', 'on antithyroid medication', 'sick',
                  'pregnant',
                  'thyroid surgery', 'I131 treatment', 'query hypothyroid', 'query hyperthyroid', 'lithium', 'goitre',
                  'tumor', 'hypopituitary', 'psych', 'TSH measured', 'TSH', 'T3 measured', 'T3', 'TT4 measured',
                  'TT4', 'T4U measured', 'T4U', 'FTI measured', 'FTI', 'TBG measured', 'TBG', REFERRAL_SOURCE_COLUMN]
Y_COL_NAME = LABEL
COLUMN_NAMES = X_COLUMN_NAMES + [Y_COL_NAME]
LABELS = [NEGATIVE, HYPO] + HYPER_LABELS


def is_hyper(label: str) -> bool:
    return label in HYPER_LABELS


def get_class_label(rows: DataFrame) -> Series:
    labels: Series = rows[LABEL]
    any_row: Series = rows.iloc[0]
    any_row[LABEL] = labels.iloc[0] if labels.iloc[0] != NEGATIVE or len(labels) < 2 else labels.iloc[1]
    return any_row


def unify_class_label(label: str) -> str:
    return HYPO if HYPO in label else NEGATIVE if NEGATIVE in label else label


def read_raw_data(file_name: str) -> DataFrame:
    return pandas.read_csv(file_name, sep=',', header=None, names=COLUMN_NAMES, index_col=False, na_values=["?"],
                           true_values='t', false_values='f')


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
@click.argument('interim_filepath', type=click.Path())
@click.argument('is_test', type=bool)
def main(input_filepath: str, output_filepath: str, interim_filepath: str, is_test: bool) -> None:
    prefix: str = TEST_PREFIX if is_test else DATA_PREFIX
    df = merge_datasets(input_filepath, interim_filepath, prefix)
    df = map_dataset(df, interim_filepath, prefix)
    impute_dataset(df, output_filepath, prefix)


def impute_dataset(df: DataFrame, output_filepath: str, prefix: str) -> DataFrame:
    imp: IterativeImputer = IterativeImputer(initial_strategy='median', tol=0.01)
    all_x_columns: Index = df.columns.difference([Y_COL_NAME, ID_COLUMN], sort=False)
    imputed_df: DataFrame = pandas.DataFrame(data=imp.fit_transform(df[all_x_columns]), columns=all_x_columns)
    imputed_df[Y_COL_NAME] = df[Y_COL_NAME].values
    imputed_df[ID_COLUMN] = df[ID_COLUMN].values
    imputed_df[BOOLEAN_COLUMNS] = imputed_df[BOOLEAN_COLUMNS].astype('boolean')
    int_columns: List[str] = [AGE_COLUMN, ID_COLUMN, SEX_COLUMN, REFERRAL_SOURCE_COLUMN]
    imputed_df[int_columns] = imputed_df[int_columns].astype(int)
    imputed_df.to_csv(output_filepath + prefix + FINAL_CSV, index=False)
    return df


def map_dataset(df: DataFrame, interim_filepath: str, prefix: str) -> DataFrame:
    df.drop(TBG_COLUMNS, axis=1, inplace=True)  # since TBG is never measured
    df[[LABEL, ID_COLUMN]] = df[LABEL].str.rsplit('.|', expand=True)
    df = df.groupby(ID_COLUMN).apply(get_class_label)
    df[LABEL] = df[LABEL].map(unify_class_label)
    df = df[df[LABEL].isin(LABELS)]
    df[CATEGORY_COLUMNS] = df[CATEGORY_COLUMNS].astype('category')
    df[SEX_COLUMN] = df[SEX_COLUMN].cat.codes.replace(-1, numpy.nan)
    df[REFERRAL_SOURCE_COLUMN] = df[REFERRAL_SOURCE_COLUMN].cat.codes.replace(-1, numpy.nan)
    df.to_csv(interim_filepath + prefix + MAPPED_CSV, index=False)
    return df


def merge_datasets(input_filepath: str, interim_filepath: str, prefix: str) -> DataFrame:
    hyper_df: DataFrame = read_raw_data(input_filepath + 'allhyper.' + prefix)
    hypo_df: DataFrame = read_raw_data(input_filepath + 'allhypo.' + prefix)
    df: DataFrame = pandas.concat([hyper_df, hypo_df]).reset_index(drop=True)
    df.to_csv(interim_filepath + prefix + MERGED_CSV, index=False)
    return df


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
