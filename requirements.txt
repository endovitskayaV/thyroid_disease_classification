# local package
-e .

# external requirements
click~=7.1.2
Sphinx
coverage
awscli
flake8
python-dotenv>=0.5.1

numpy~=1.19.2
pandas~=1.1.3
scikit-learn~=0.24.2
pip~=20.2.4
zlib~=1.2.11
wheel~=0.35.1
openssl~=1.1.1k
cryptography~=3.1.1
py~=1.9.0
lxml~=4.6.1
tornado~=6.0.4
keyring~=21.4.0
pkginfo~=1.6.1
setuptools~=50.3.1
cython~=0.29.21
future~=0.18.2
docutils~=0.15.2
statsmodels~=0.12.0
joblib~=0.17.0
imblearn~=0.0
matplotlib~=3.3.2